# Spectrometer

use `npm install .` or `npm install . --force` to install dependencies and 
start with `npm start`.

This project is meant to work with hololinked server side 
[spectrometer example](https://gitlab.com/hololinked-examples/oceanoptics-spectrometer) 

Start the device server and search for it in the app-bar to interact with it.  



